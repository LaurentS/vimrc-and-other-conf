#!/bin/bash

# run this after laptop comes out of hibernation/snooze
# to restore proper network connectivity. Somehow protonvpn
# and nextdns don't seem to play nicely together without
# this
sudo service nextdns restart
# protonvpn-cli connect -f
protonvpn-cli r

sudo service nextdns restart
