-- set the <leader> key to comma, easier to reach than the default backslash
vim.g.mapleader = ","

-- be iMproved, required
vim.opt.compatible = false
vim.opt.rtp = vim.opt.rtp + "~/.fzf"
vim.opt.filetype = "off"
vim.opt.number = true
vim.opt.relativenumber = true

-- highlight weird characters
vim.opt.list = true
vim.opt.listchars = "tab:|.,trail:•,extends:»,nbsp:¬"

vim.opt.ruler = true

vim.opt.showmode = true
vim.opt.showcmd = true
-- not used, lualine takes care of this
-- vim.opt.statusline = '%<%f\ %h%m%r%=%-20.(line=%l,col=%c%V,totlin=%L%)\%h%m%r%=%-40(,bytval=0x%B,%n%Y%)\%P'
vim.opt.smartcase = true

-- allow modelines at top of files
vim.opt.modeline = true

-- set neovim to use its own python virtualenv, so it doesn't get polluted by
-- local varieties of packages :)
-- neovim python provider using pyenv as detailed in
-- https://neovim.io/doc/user/provider.html (under "python virtualenvs")
vim.g.python3_host_prog = "/home/laurent/.pyenv/versions/py3nvim/bin/python"

-- set python coding standards
-- au FileType python setlocal tabstop=4 expandtab shiftwidth=4 softtabstop=4

-- scrolling up/down
vim.api.nvim_set_keymap("n", "<Space>", "<PageDown>", { noremap = true })
vim.api.nvim_set_keymap("n", "<BS>", "<PageUp>", { noremap = true })
-- escape clears the search highlight
vim.api.nvim_set_keymap("n", "<esc>", ":noh<return><esc>", { noremap = true })

-- search all files in current folder
vim.api.nvim_set_keymap("n", "<C-f>", "<cmd>lua require('fzf-lua').files()<CR>", { silent = true })
vim.api.nvim_set_keymap("i", "<C-f>", "<cmd>lua require('fzf-lua').blines()<CR>", { silent = true })
-- search lines in loaded buffers
vim.api.nvim_set_keymap("n", "<C-g>", "<ESC><ESC>:Lines!<CR>", {})
-- ripgrep through the folder
vim.api.nvim_set_keymap("n", "<C-d>", "<cmd>lua require('fzf-lua').grep_project()<CR>", { silent = true })

-- navigate splits with C-hjkl instead of the default C-w hjkl
-- nmap <silent> <c-a-h> :wincmd h<CR>
vim.api.nvim_set_keymap("n", "<c-a-h>", ":wincmd h<CR>", {})
-- nmap <silent> <c-a-j> :wincmd j<CR>
vim.api.nvim_set_keymap("n", "<c-a-j>", ":wincmd j<CR>", {})
-- nmap <silent> <c-a-k> :wincmd k<CR>
vim.api.nvim_set_keymap("n", "<c-a-k>", ":wincmd k<CR>", {})
-- nmap <silent> <c-a-l> :wincmd l<CR>
vim.api.nvim_set_keymap("n", "<c-a-l>", ":wincmd l<CR>", {})

vim.opt.clipboard = vim.opt.clipboard + "unnamedplus"
vim.opt.mouse = "a"

-- Zeavim settings
vim.g.zv_file_types = {
    sql = "psql",
    py = "python,sqlalchemy",
}

-- vim.cmd([[
--
-- " set guifont=DejaVuSansMono\ Nerd\ Font\ Mono\ Book\ 10
-- set go-=T  " hide toolbar
-- set go-=r  " hide right hand scrollbar
-- set go-=L  " hide left hand scrollbar
-- set go-=m  " hide menu
-- "system('wmctrl -i -b add,maximized_vert,maximized_horz -r '.v:windowid)
--
-- " TagList options
-- let Tlist_Ctags_Cmd='/usr/local/bin/ctags'
-- let Tlist_Exit_OnlyWindow = 1
-- nnoremap <silent> <F6> :TlistToggle<CR>
--
-- "------------------------------------------------------------------------------
-- " custom keymaps
-- "------------------------------------------------------------------------------
--
-- " junegunn/FZF/fzf keymaps
-- " replace the default command with 'find .' to include hidden files but ignore
-- " __pycache__ directories
-- command! -bang -nargs=? -complete=dir Files
--   \ call fzf#vim#files(<q-args>, {'source': 'find . \( -name *__pycache__* -o -name *.mypy_cache* \) -prune -o -print'}, <bang>0)
--
-- "------------------------------------------------------------------------------
-- " javascript mode
-- "------------------------------------------------------------------------------
-- " consider .js files as jsx so mxw/vim-jsx will handle them too
-- let g:jsx_ext_required = 0
-- " set indents to 4 chars, expand to spaces
-- augroup JSProjectSettings
-- au FileType javascript.jsx setlocal expandtab tabstop=2 shiftwidth=2 autoindent smartindent
-- autocmd BufNewFile,BufRead,BufEnter /home/laurent/Sites/mapswipe/*.js set ft=javascript.jsx expandtab tabstop=4 shiftwidth=4 autoindent smartindent
-- autocmd BufNewFile,BufRead,BufEnter /home/laurent/Sites/openfunds/ofd_site/*.js set ft=javascript.jsx expandtab tabstop=4 shiftwidth=4 autoindent smartindent
-- autocmd BufNewFile,BufRead,BufEnter /home/laurent/Sites/ovio/*.js set ft=javascript expandtab tabstop=2 shiftwidth=2 autoindent smartindent
-- autocmd BufNewFile,BufRead,BufEnter /home/laurent/Sites/dada/dada-core/*.js set ft=javascript expandtab tabstop=2 shiftwidth=2 autoindent smartindent
-- autocmd BufNewFile,BufRead,BufEnter /home/laurent/Sites/dada/dada-core/*.html set ft=html expandtab tabstop=4 shiftwidth=4 autoindent smartindent
-- augroup END
--
-- " this should be in the ftplugin/typescript file, but somehow it's not working
-- " :(
-- au FileType typescript setlocal expandtab tabstop=2 shiftwidth=2 autoindent smartindent
--
-- """""""""""""""""""""""""""""""""""""""""""""""""
-- " read manpages in vim buffer with :Man xxx
-- runtime! ftplugin/man.vim
--
-- " add a Scratch command to open a scratch buffer (linked to no file, deleted on quitting vim)
-- " src: https://vi.stackexchange.com/a/27423
-- command! Scratch new | setlocal bt=nofile bh=wipe nobl noswapfile nu
-- ]])

vim.opt.termguicolors = true
vim.opt.background = "dark"

vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

switchbg = function()
    if vim.o.background == "light" then
        vim.o.background = "dark"
    else
        vim.o.background = "light"
    end
end

vim.keymap.set("n", "<leader>t", switchbg, { desc = "Switch background color and them dark/light" })

vim.api.nvim_create_autocmd({ "Signal" }, {
    callback = function()
        switchbg()
    end,
})

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- local use = require("packer").use
require("lazy").setup({
    -- use("wbthomason/packer.nvim") -- Package manager
    "neovim/nvim-lspconfig",               -- Collection of configurations for built-in LSP client
    "williamboman/mason.nvim",             -- easily install LSP tools
    "williamboman/mason-lspconfig.nvim",   -- help link mason and lsp-config
    "hrsh7th/nvim-cmp",                    -- Autocompletion plugin
    "hrsh7th/cmp-nvim-lsp",                -- LSP source for nvim-cmp
    "hrsh7th/cmp-nvim-lsp-signature-help", -- show function signatures
    "hrsh7th/cmp-path",                    -- LSP source for nvim-cmp for paths
    -- vscode css installed through mason
    -- "hrsh7th/vscode-langservers-extracted", -- LSP for CSS/HTML...
    -- use 'altercation/vim-colors-solarized'
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
    },
    "tpope/vim-fugitive",
    { "junegunn/fzf", build = "./install --bin" },
    {
        "ibhagwan/fzf-lua",
        -- optional for icon support
        dependencies = { "nvim-tree/nvim-web-devicons" },
        config = function()
            -- calling `setup` is optional for customization
            require("fzf-lua").setup({})
        end,
    },
    "junegunn/fzf.vim",
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "kyazdani42/nvim-web-devicons", lazy = true },
    },
    "nvim-lua/plenary.nvim",  -- various utility functions, used by none-ls
    "nvimtools/none-ls.nvim", -- make various non-lsp linters and formatters LSP-compatible

    "tomtom/tcomment_vim",

    "KabbAmine/zeavim.vim",

    "ray-x/cmp-treesitter",
    -- pick one of the 4 variants of solarized theme
    -- high contrast has a bug: https://github.com/ishan9299/nvim-solarized-lua/issues/43
    -- vim.cmd('colorscheme solarized-flat')
    -- vim.cmd('colorscheme solarized-low')  -- low contrast variant
    -- vim.cmd('colorscheme solarized-high')  -- high contract variant
    {
        -- from https://github.com/ishan9299/nvim-solarized-lua/issues/29
        "ishan9299/nvim-solarized-lua",
        config = function()
            vim.g.solarized_italics = 1
            vim.cmd("colorscheme solarized") -- normal variant
        end,
    },
    {
        "L3MON4D3/LuaSnip",
        -- follow latest release.
        version = "v2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
        -- install jsregexp (optional!).
        build = "make install_jsregexp",
    },
})

require("mason").setup({
    log_level = vim.log.levels.DEBUG,
})
require("mason-lspconfig").setup()

-- vim.lsp.set_log_level("debug")

-- tree-sitter
require("nvim-treesitter.configs").setup({
    -- A list of parser names, or "all"
    ensure_installed = {
        "bash",
        "css",
        "dockerfile",
        "gitignore",
        "go",
        "graphql",
        "html",
        "javascript",
        "json",
        "lua",
        "make",
        "markdown",
        "nix",
        "python",
        "ruby",
        "rust",
        "sql",
        "svelte",
        "toml",
        "typescript",
        "xml",
        "yaml",
    },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- List of parsers to ignore installing (for "all")
    -- ignore_install = { "javascript" },

    highlight = {
        -- `false` will disable the whole extension
        enable = true,

        -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
        -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
        -- the name of the parser)
        -- list of language that will be disabled
        -- disable = { "c", "rust" },

        -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
        -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
        -- Using this option may slow down your editor, and you may see some duplicate highlights.
        -- Instead of true it can also be a list of languages
        additional_vim_regex_highlighting = false,
    },
})

local opts = { noremap = true, silent = true }
-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
-- src: https://github.com/hrsh7th/cmp-nvim-lsp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

local run_in_file_context = function(cmd)
    -- for python files: cd to the folder of the file to run poetry
    -- with the correct context (for black, isort...)
    -- Returns the command with "cd xxx && cmd" prefixed
    file_path = vim.fn.getcwd() .. "/" .. vim.fn.expand("%")
    file_dir = file_path:match("(.*/)")
    return "cd " .. file_dir .. " && " .. cmd
end

-- Local func to check if formatting is enabled for none-ls handlers
local is_lsp_formatting_enabled = function(bufnr, _)
    local ok, result = pcall(vim.api.nvim_buf_get_var, bufnr, "lsp_enabled")
    -- No buffer local variable set, so just enable by default
    return true
end
--     if not ok then
--         return true
--     end
--     return result
-- end

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Mappings.
    -- setup keybindings to navigate error reports
    local opts = { noremap = true, silent = true }
    vim.keymap.set("n", "<C-k>", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)
    vim.keymap.set("n", "<C-j>", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)
    vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, opts)
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    -- if client.supports_method("textDocument/declaration") then
    vim.keymap.set("n", "<leader>D", vim.lsp.buf.declaration, bufopts)
    -- end
    -- if client.supports_method("textDocument/definition") then
    vim.keymap.set("n", "<leader>d", vim.lsp.buf.definition, bufopts)
    -- end

    -- setup autoformatting with none-ls, src:
    -- https://github.com/nvimtools/none-ls.nvim/wiki/Formatting-on-save#sync-formatting
    if client.supports_method("textDocument/formatting") then
        vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
        vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function()
                -- if is_lsp_formatting_enabled(bufnr) then
                vim.lsp.buf.format({ bufnr = bufnr })
                -- end
            end,
        })
    end
end

local lsp_config_util = require("lspconfig.util")

-- html , css LSPs
local css_capabilities = vim.lsp.protocol.make_client_capabilities()
css_capabilities.textDocument.completion.completionItem.snippetSupport = true

require("lspconfig").cssls.setup({
    capabilities = css_capabilities,
})
require("lspconfig").html.setup({
    capabilities = css_capabilities,
})

require("lspconfig").flow.setup({ on_attach = on_attach })
-- require("lspconfig").marksman.setup({ on_attach = on_attach })
require("lspconfig").taplo.setup({ on_attach = on_attach })
require("lspconfig").docker_compose_language_service.setup({ on_attach = on_attach })
require("lspconfig").dockerls.setup({ on_attach = on_attach })
require("lspconfig").jsonls.setup({ on_attach = on_attach })
require("lspconfig").rust_analyzer.setup({ on_attach = on_attach })
require("lspconfig").ansiblels.setup({ on_attach = on_attach })
require("lspconfig").nil_ls.setup({ on_attach = on_attach }) -- nix lsp
require("lspconfig").solargraph.setup({
    -- root_dir is a func that returns true when it is given a directory
    -- that can work as a root for the project. Look for a Gemfile as our madada
    -- theme folder does not hold one. The dada-core git repo should be under
    -- alaveteli/dada-core for this to work
    root_dir = lsp_config_util.root_pattern("Gemfile"),
    on_attach = on_attach,
})
require("lspconfig").svelte.setup({ on_attach = on_attach })
require("lspconfig").ts_ls.setup({ on_attach = on_attach })
require("lspconfig").lua_ls.setup({ on_attach = on_attach })
require("lspconfig").prolog_ls.setup({ on_attach = on_attach })
require("lspconfig").ruff.setup({ on_attach = on_attach })

-- manually install rubocop plugins for alaveteli dev
-- src: https://github.com/williamboman/mason.nvim/issues/392#issuecomment-1585012266
-- This works, and installs the 2 rubocop extensions below (performance and rails)
local function mason_package_path(package)
    local path = vim.fn.resolve(vim.fn.stdpath("data") .. "/mason/packages/" .. package)
    return path
end

require("plenary.job")
    :new({
        command = "gem",
        args = {
            "install",
            "--no-user-install",
            "--no-format-executable",
            "--install-dir=.",
            "--bindir=bin",
            "--no-document",
            "rubocop",
            "rubocop-performance",
            "rubocop-rails",
        },
        cwd = mason_package_path("rubocop"),
    })
    :start()

-- end manually install rubocop

local null_ls = require("null-ls")
local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
null_ls.setup({
    sources = {
        null_ls.builtins.diagnostics.ansiblelint,
        -- null_ls.builtins.diagnostics.eslint_d,  replaced by eslint-d lsp?  NOT SETUP YET
        null_ls.builtins.diagnostics.hadolint,
        -- null_ls.builtins.formatting.eslint_d,  replaced by eslint-d lsp?
        -- use local black if there is one, so it uses the right version, conf...
        null_ls.builtins.formatting.black.with({ prefer_local = ".venv/bin" }),
        null_ls.builtins.formatting.isort.with({ prefer_local = ".venv/bin" }),
        -- null_ls.builtins.formatting.htmlbeautifier,  use tidy instead, see https://github.com/nvimtools/none-ls.nvim/issues/58
        -- install nixfmt with `nix profile install nixpkgs#nixfmt-rfc-style`
        null_ls.builtins.formatting.nixfmt,
        null_ls.builtins.formatting.pg_format,
        null_ls.builtins.formatting.prettierd,
        null_ls.builtins.formatting.rubocop,
        null_ls.builtins.formatting.stylua,
        null_ls.builtins.formatting.yamlfmt,
        null_ls.builtins.diagnostics.yamllint,
        null_ls.builtins.diagnostics.rubocop,
        null_ls.builtins.completion.luasnip,
        -- null_ls.builtins.diagnostics.sqlfluff.with({
        --     extra_args = { "--dialect", "postgres" }, -- change to your dialect
        -- }),
        -- null_ls.builtins.formatting.sql_formatter,
    },
    on_attach = on_attach,
})

local lsp_flags = {
    -- This is the default in Nvim 0.7+
    debounce_text_changes = 50,
}

local find_python_venv_for_file = function()
    local p
    if vim.env.VIRTUAL_ENV then
        -- should this be lsp_config_util?? and same below? though seems to work
        p = lsp_util.path.join(vim.env.VIRTUAL_ENV, "bin", "python3")
    else
        p = utils.find_cmd("python3", ".venv/bin", config.root_dir)
    end
    if p ~= "" then
        local venv = vim.fn.trim(vim.fn.system("poetry --directory " .. workspace .. " env info -p"))
        p = path.join(venv, "bin", "python")
    end
    return p
end

require("lspconfig")["pyright"].setup({
    on_attach = on_attach,
    flags = lsp_flags,
    -- point pyright to use the correct venv
    -- source: https://github.com/neovim/nvim-lspconfig/issues/500#issuecomment-965824580
    before_init = function(_, config)
        p = find_python_venv_for_file()
        config.settings.python.pythonPath = p
        config.settings.python.analysis.typeCheckingMode = "strict"
        -- root_dir = vim.fs.dirname(vim.fs.find({'setup.py', 'pyproject.toml'}, { upward = true })[1]),
    end,
})

require("lualine").setup({
    -- themes list: https://github.com/nvim-lualine/lualine.nvim/blob/master/THEMES.md
    options = { theme = "solarized_dark" },
    sections = {
        -- override last section to include total number of lines in file
        lualine_z = { { "%l:%c / %L" } },
    },
})
-- Register a handler that will be called for each installed server when it's ready (i.e. when installation is finished
-- or if the server is already installed).
-- :LspInstallInfo to i/u/U/x lsp servers
-- lsp_installer.on_server_ready(function(server)
-- 	local lspconfig = require 'lspconfig'
-- 	-- these options are passed to all servers
-- 	local opts = {
--
-- 	-- prevent tsserver from starting on flow projects (MapSwipe)
-- 	if server.name == "tsserver" then
-- 		opts.root_dir = function(fname)
-- 			return lspconfig.util.root_pattern('tsconfig.json')(fname)
-- 			    or not lspconfig.util.root_pattern('.flowconfig')(fname)
-- 			    and lspconfig.util.root_pattern('package.json', 'jsconfig.json', '.git')(fname)
-- 		end
-- 	end
--
-- 	-- This setup() function will take the provided server configuration and decorate it with the necessary properties
-- 	-- before passing it onwards to lspconfig.
-- 	-- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
-- 	server:setup(opts)
-- end)

-- nvim-cmp setup
local cmp = require("cmp")
cmp.setup({
    mapping = {
        ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-n>"] = cmp.mapping.select_next_item(),
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.close(),
        ["<CR>"] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            -- select=false prevents completion if nothing was highlighted
            select = false,
        }),
        ["<Tab>"] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
                -- elseif luasnip.expand_or_jumpable() then
                --   luasnip.expand_or_jump()
            else
                fallback()
            end
        end,
        ["<S-Tab>"] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
                -- elseif luasnip.jumpable(-1) then
                --   luasnip.jump(-1)
            else
                fallback()
            end
        end,
    },
    sources = {
        { name = "nvim_lsp" },
        { name = "nvim_lsp_signature_help" },
        { name = "treesitter" },
        -- { name = 'luasnip' },
    },
})

-- force autodetection of poetry.lock as toml files
vim.cmd([[
  au BufNewFile,BufRead poetry.lock set ft=toml
]])
