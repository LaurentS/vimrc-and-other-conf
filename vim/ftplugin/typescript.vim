" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

setlocal expandtab tabstop=2 shiftwidth=2 autoindent smartindent
