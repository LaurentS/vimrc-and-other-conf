" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

setlocal tabstop=4 expandtab shiftwidth=4 softtabstop=4 smarttab
