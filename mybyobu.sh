#!/bin/bash

# This starts a set of byobu sessions and windows
# Just run this script from alacritty to have a clean set of windows :)

# this fixes the color and italics problems
export BYOBU_TERM=alacritty

MAIN_SESSION=dada
# 2019-04-10: commented byobu list-sessions as it crashed with 'no server running at /tmp/tmux-1000/default'
if [ -z "$(byobu list-sessions | grep $MAIN_SESSION)" ]
then
  # name the current session dada
  # byobu rename dada
  byobu-tmux new-session -d -t $MAIN_SESSION # creates a new detached Byobu session
  # byobu-tmux rename-window runserver # Byobu comes up with one window, so rename that
  byobu-tmux send-keys -t 0 'cd ~/Sites/dada/alaveteli/dada-core' 'C-m'
  byobu-tmux new-window
  byobu-tmux send-keys -t 1 'cd ~/Sites/dada/alaveteli/dada-core' 'C-m'

  # Create a session for ovio dev
  byobu-tmux new-session -s ovio -d
  # byobu-tmux attach-session -t ovio
  byobu-tmux send-keys -t ovio:0 'cd ~/Sites/ovio/ovio-xplore-app/packages/data-ingestion' 'C-m'
  # byobu-tmux new-window -t ovio
  # byobu-tmux new-window -t ovio

  # another for mapswipe
  byobu-tmux new-session -s postmanaged -d
  byobu-tmux send-keys -t postmanaged:0 'cd ~/Sites/postmanaged' 'C-m'
  byobu-tmux new-window -t postmanaged:0
  byobu-tmux new-window -t postmanaged
  byobu-tmux send-keys -t postmanaged:1 'cd ~/Sites/postmanaged' 'C-m'

  # another for wherethefund
  byobu-tmux new-session -s wtf -d
  byobu-tmux send-keys -t wtf:0 'cd ~/Sites/openfindata/' 'C-m'
  # byobu-tmux send-keys -t wtf:0 'poetry shell' 'C-m'
  # byobu-tmux new-window -t wtf

  # and one more term session for random stuff
  byobu-tmux new-session -s zzz -d
fi
# Enter Byobu
byobu-tmux attach -t $MAIN_SESSION

# the config files for this are under ./.byobu/*
# read the scripts in `dpkg -L byobu` (in /usr/lib/byobu and /usr/share/byobu)
# and /usr/bin/byobu for details on how this works
