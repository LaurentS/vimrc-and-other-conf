# my restic setup for backups

Load restic env vars:
```
#!/bin/bash
# export env vars needed by restic
export RESTIC_REPOSITORY="path_to_storage"
export AWS_ACCESS_KEY_ID=""
export AWS_SECRET_ACCESS_KEY=""
export RESTIC_PASSWORD=""
```
Run `restic --verbose backup ~ --exclude-file=~/myconf/restic-excludes.txt` to run a backup. This is incremental.

Files to exclude are in restic-excludes.txt
