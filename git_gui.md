Follow instructions at http://blog.serindu.com/2019/03/07/applying-tk-themes-to-git-gui/
I've used https://github.com/MaxPerl/ttk-Breeze
There is a collection of themes at https://github.com/TkinterEP/ttkthemes

## Copied here just in case:

Stashing this here for my own future information since it took me some time to figure out. How to change how Git Gui / Gitk look using different TK themes and how to install and use an external TK theme (such as one of these: https://wiki.tcl-lang.org/page/List+of+ttk+Themes).
How to Check Installed Themes

In Linux, you probably already have everything you need to adjust the TK theme used by Git Gui without installing anything new. To check what themes are available we can use the TK tool "wish". Running "wish" will popup a window into which you can build widgets, we'll just ignore it for our purposes.

We can check the TK version with "info patchlevel" and then list out the built-in themes with "ttk::style theme names":

$ wish
% info patchlevel
8.6.8
% ttk::style theme names
clam alt default classic
% exit
$

We currently have the built-in themes: clam, alt, default, and classic
How to Switch Themes

We can temporarily switch the theme to see if we like it using this magic:
echo '*TkTheme: [insert theme name here]' | xrdb -merge -
For example:

$ echo '*TkTheme: clam' | xrdb -merge -

And then if we run git gui it will be styled slightly differently than the default:

$ git gui

To make the change stick we need to adjust our user settings. Create or edit the file .Xresources in your home directory:

$ nano ~/.Xresources

And add to that file:

*TkTheme: clam

Now when you reboot (or restart the X server), git gui will use the "clam" theme.
How to Install External TK Theme

I grabbed the awthemes.tcl file from the above-linked list of themes. Awthemes provides the "awdark" and "awlight" themes. But then you have to figure out what to do with it to actually use them.

First, let's stash it somewhere useful:

$ mkdir ~/.local/share/tk-themes
$ mkdir ~/.local/share/tk-themes/awthemes
$ mv ~/Downloads/awthemes.tcl ~/.local/share/tk-themes/awthemes/

And now we create a package index file:

$ nano ~/.local/share/tk-themes/awthemes/pkgIndex.tcl

And put in there:

package ifneeded ttk::theme::awdark 2.3 [list source [file join $dir awthemes.tcl]]

Make sure you match the version number you put into pkgIndex.tcl to the one set in awthemes.tcl.

And, finally, we need to let TCL know where to find our external themes. We do this by setting an environment variable in our profile.

$ nano ~/.profile

Add the following to the end of that file:

# TK Themes
export TCLLIBPATH=$HOME/.local/share/tk-themes

Now logout and log back in (or use "source ~/.profile").

In order to see our theme we have to request it so that TK will go look for it:

$ echo '*TkTheme: awdark' | xrdb -merge -

If we check our installed themes again we'll see the "awdark" and "awlight" themes are now available.

$ wish
% ttk::style theme names
awlight clam alt default awdark classic
% exit
$

If we set the theme in ~/.Xresources it will be found and loaded when the X server starts.

$ nano ~/.Xresources

And change it to:

*TkTheme: awdark

Reboot your machine and when you log back in you should be using the new theme when you run git gui or gitk.

Unfortunately, I have not yet figured out how to adjust the ListBoxes and Text widgets using the
::ttk::theme::awdark::setTextColors and
::ttk::theme::awdark::setListboxColors procs as described in the awthemes documentation.
