#!/bin/bash

# this script switches the alacritty config between a dark and a light
# colour scheme. The entire config must be duplicated right now, which
# isn't super nice

# Usage: just call ./swap_colors.sh
# there is a keyboard shortcut in the alacritty conf files to call
# this script.
# Note: edits in alacritty.yml will be overwritten when the script is run.
# so edit the alacritty_light.yml and alacritty_dark.yaml files.

OLDWD=`pwd`
cd ~/.config/alacritty

ALACONF=alacritty.toml

LAZYGITCONF=../lazygit/config.yml

if rg -q solarized_light $ALACONF; then
    sed -i 's/solarized_light/solarized_dark/' $ALACONF
    sed -i '/# DARK$/s/^    # p/    p/' $LAZYGITCONF
    sed -i '/# LIGHT$/s/^    p/    # p/' $LAZYGITCONF
else
    sed -i 's/solarized_dark/solarized_light/' $ALACONF
    sed -i '/# DARK$/s/^    p/    # p/' $LAZYGITCONF
    sed -i '/# LIGHT$/s/^    # p/    p/' $LAZYGITCONF
fi

# send SIGUSR1 to neovim, which calls the switchbg autocmd
kill -USR1 `pidof vi`
cd $OLDWD
