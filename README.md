# Notes about other bits of config that can help

## DNS - slow ping

2021-10-14: remove "nameserver fe80::1%slp3s0" from /etc/resolv.conf improved ping time from 3s to "instant"

protonvpn does not support ipv6, they just block traffic on it. At this point, protonvpn+nextDNS does leak the dns, but
it just shows nextDNS, so I'll keep using this. 

## Problems with protonvpn

- port 5432 is randomly blocked, no docs about it
- ipv6 traffic blocked, only shown in a random blog post
- docs are a bit limited, messy
- support is not fantastic :(

On the plus side:
- tons of countries
- good speed (almost as fast as raw connection)
- does not block sites like netflix (do I care?)
