#!/bin/bash

# This script runs the restic backup.
# it is meant to be called from a cron job, with no parameters

source /home/laurent/myconf/restic-env.sh
# export env vars needed by restic, the env file should contain the following lines
#export RESTIC_REPOSITORY=""
#export AWS_ACCESS_KEY_ID=""
#export AWS_SECRET_ACCESS_KEY=""
#export RESTIC_PASSWORD=""

RESTIC=/usr/local/bin/restic

$RESTIC -o s3.list-objects-v1=true --verbose backup /home/laurent/ --exclude-file=/home/laurent/myconf/restic_excludes.txt

# see https://restic.readthedocs.io/en/latest/060_forget.html
$RESTIC -o s3.list-objects-v1=true --verbose forget --keep-last 10 --keep-daily 7 --keep-weekly 4 --keep-monthly 12 --keep-yearly 3

# actually remove the forgotten data from the remote repository
$RESTIC -o s3.list-objects-v1=true prune
