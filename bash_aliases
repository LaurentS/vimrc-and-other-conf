alias lg='lazygit'
alias gk='gitk --all'
alias ll='lsd -l'
# set the TERM env var so that ssh behaves normally from within
# alacritty. See https://github.com/alacritty/alacritty/issues/6316
alias ssh='TERM=xterm-256color ssh'
