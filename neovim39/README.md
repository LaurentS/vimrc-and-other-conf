# Virtual env setup for neovim

in ~/.vimrc:
let g:python3_host_prog = '/path/to/virtualenv/bin/python3'

then manage packages needed in this project, using poetry, the list is in pyproject.toml
