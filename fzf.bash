# Setup fzf
# ---------
if [[ ! "$PATH" == */home/laurent/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/laurent/.fzf/bin"
fi

# eval "$(fzf --bash)"
# See comments at the top of that file
eval "$(cat /home/laurent/myconf/bash_fzf_keybindings.bash)"
