# Notes about making frame.work... work :)

https://wiki.debian.org/InstallingDebianOn/FrameWork/12thGen
has a bunch of good info.

## Brightness keys

They don't work out of the box (on F7, F8), so followed this:
https://community.frame.work/t/responded-12th-gen-not-sending-xf86monbrightnessup-down/20605/28

sudo nano /etc/modprobe.d/framework-als-blacklist.conf
=> it creates an empty file.
Then add:
blacklist hid_sensor_hub
Then do :
sudo update-initramfs -u
